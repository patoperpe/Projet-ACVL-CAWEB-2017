

This file explains how to configure the applicaction in order to deploy it in your PC or cloud.

First, there are some list of needs that you need to cover. The list is:
	-Tomcat installed.
	-Maven installed.
	-Oracle library in the libs section of Tomcat. It can be download in Oracle's official web page. (http://www.oracle.com/technetwork/apps-tech/jdbc-112010-090769.html)
	-Oracle SQL server available. For this, you can use our account on Ensimag University (you need to be connected to Ensimag VPN network) or you can use your own database by modifying the file context.xml and put your server configurations.

Once you have done with the list, you have to create sql tables in your server. To do this, you just run "install.db" file with any sql editor you want. Then, you need to unzip de file target.zip. Then you open a terminal inside the folder target and type the command "mvn tomcat6:deploy". 
Note that 6 is for tomcat version 6. if you have another, you must change it.

That's it! Now you can create your users and start enjoying the game.

Project ACVL/CAWEB 2017
Authors:
	ANTONINI Anna Paola
	FITE Juan Manuel
  	PERPETUA Patricio Rafael
   	VI�AS Dardo Ariel
