<%-- 
    Document   : header
    Created on : Apr 4, 2017, 10:15:36 AM
    Author     : perpetup
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <header>
        <img src="Resourses/Images/villager2.png" alt="Werewolf" style="width:64px;height:64px;">
        <img src="Resourses/Images/favicon.png" alt="Werewolf" style="width:64px;height:64px;">
        <img src="Resourses/Images/villager1.png" alt="Werewolf" style="width:64px;height:64px;">
        <h1>Werewolf - The Game</h1>
    </header>
    <nav class="nav">
        <div class="btn-group">
            <%
                out.print("<form action=\"controller_Session\">");
                out.print("<input type=\"hidden\" value=\"rules\" name=\"view\">");
                out.print("<button type=\"submit\">Rules</button>");
                out.print("</form>");
                out.print("<br>");
                out.print("<form action=\"controller_Session\">");
                out.print("<input type=\"hidden\" value=\"about\" name=\"view\">");
                out.print("<button type=\"submit\">About</button>");
                out.print("</form>");
                out.print("<br>");
                HttpSession oSession = request.getSession();
                if (oSession != null && request.getSession().getAttribute("username") != null) {
                    out.print("<form action=\"controller_Session\">");
                    out.print("<input type=\"hidden\" value=\"logOut\" name=\"action\">");
                    out.print("<button type=\"submit\">LogOut!</button>");
                    out.print("</form>");
                    out.print("<br>");
                    out.print("<form action=\"Controller_Match\">");
                    out.print("<button type=\"submit\">Main Menu</button>");
                    out.print("</form>");
                }else{
                    out.print("<form action=\"controller_Session\">");
                    out.print("<input type=\"hidden\" value=\"logIn\" name=\"view\">");
                    out.print("<button type=\"submit\">LogIn!</button>");
                    out.print("</form>");
                }
            %>
            <input type="hidden" value="logOut" name="action">
        </div>
    </nav>
</html>
