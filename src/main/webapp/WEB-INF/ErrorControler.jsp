<?xml version="1.0" encoding="UTF-8"?>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <title>Controler Error</title>
    </head>
    <body>
        <h1 style="text-align: center">Controler Error</h1>
        <img src="images/scared.png"/><br/>
        Invalid parameters: action <span style="color: red">${param.action}</span>
        and view <span style="color: red">${param.view}</span>
    </body>
</html>
