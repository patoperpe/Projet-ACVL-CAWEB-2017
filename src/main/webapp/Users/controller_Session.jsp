<%@page import="model.Users"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<html>
    <head>
        <link rel="icon" href="Resourses/Images/favicon.png">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>Welcome</title>
        <script>
            <% 
                ArrayList<Users> lUsers = (ArrayList<Users>) request.getAttribute("list");
                if(lUsers == null)
                    lUsers = new ArrayList<Users>();
            %>
            var usernames =  new Array();
                <% for (int i=0; i<lUsers.size(); i++) { %>
                    usernames[<%= i %>] = "<%= lUsers.get(i).getsUsername() %>"; 
                <% } %>
        </script>
        <script type="text/javascript" src="JS/script.js"></script>
    </head>
    <body>
        <div class="flex-container">
            <jsp:include page="../header.jsp"/>
            <article class="article">
                <form method="POST" action="controller_Session?action=add" onSubmit="return checkForm(this)">
                    <p>Username: <input type="text" name="username"></p>
                    <p>Password: <input type="password" name="pwd1"></p>
                    <p>Confirm Password: <input type="password" name="pwd2"></p>
                    Your password must have a minimum of 6 characters.
                    <p><input type="submit"></p>
                </form>
            <a href="controller_Session?view=logIn" >LogIn!</a>
            </article>


        </div>
            <jsp:include page="../footer.jsp"/>
    </body>
</html>