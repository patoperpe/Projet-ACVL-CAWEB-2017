<%-- 
    Document   : gameFinish
    Created on : Apr 11, 2017, 10:44:16 AM
    Author     : perpetup
--%>

<%@page import="Utils.Dates"%>
<%@page import="model.Players"%>
<%@page import="model.Chats"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Matches"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="JS/script.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>WereWolf - Game Finished</title>
    </head>
    <body>
        <jsp:include page="../header.jsp"/>
        <article class="article">
            <%
                Matches oMatch = (Matches) request.getSession().getAttribute("thisMatch");
                ArrayList<Chats> listChats = (ArrayList<Chats>) request.getAttribute("listChats");
                ArrayList<Players> listPlayers = (ArrayList<Players>) request.getAttribute("listPlayers");
                int iWolfs = 0, iLivePlayers = 0, iLiveWolf = 0;
            %>
            <div>
                <div class="column-left">
                    <p>Users</p>
                    <ul>
                    <%
                        if(listPlayers != null){
                        for(Players oPla : listPlayers){
                            out.print("<li>" + oPla.getoUser().getsUsername());
                            if (oPla.isIsAlived()) {
                                out.print(" - " + " ALIVE" + "</li>");
                            } else {
                                out.print(" - " + "DEAD" + "</li>");
                            }
                            if(oPla.isIsAlived())
                                iLivePlayers++;
                            if(oPla.isIsWolf()){
                                iWolfs++;
                                if(oPla.isIsAlived())
                                    iLiveWolf++;
                            }
                        }
                        out.print("</ul>");
                        out.print("<p>" + "Number of wolves at the begining: "+ iWolfs+"</p>");
                        out.print("<p>" + "Number of live wolves: "+ iLiveWolf+"<p>");
                        out.print("<p>" + "Number of live citizenss: "+ iLivePlayers+"<p>");
                        if(iLivePlayers> iLiveWolf)
                            out.print("<h2>" + "Citizens won!"+"</h2>");
                        else
                            out.print("<h2>" + "Wolves won!"+"</h2>");
                    }
                    %>
                </div>
                <div class="column-center">
                    <p>Archived Chats</p>
                    <ul>
                        <%
                            if (listChats != null && !listChats.isEmpty()) {
                                for (Chats oChats : listChats) {
                                    out.print("<li>" + Dates.sdDays.format(oChats.getoDate()));
                                    if(oChats.isIsForWolf())
                                        out.print(" - Wolf");
                                    else
                                        out.print(" - Citizen");
                                    out.print(" - "+"<a target=\"_blank\" href=\"Controller_Game?idMatches="
                                        +oMatch.getiID()+"&view=chat&idChats="
                                        + oChats.getiID() + "\">See Chat!</a>" + "</li>");
                                }
                            } else {
                                out.print("<p>" + "There is not previous chats!" + "</p>");
                            }
                        %>
                    </ul>
                </div>
            </div>

        </article>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
