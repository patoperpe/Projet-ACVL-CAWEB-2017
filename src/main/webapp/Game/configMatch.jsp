<%-- 
    Document   : configMatch
    Created on : 30/03/2017, 15:37:06
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../WEB-INF/error.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" href="Resourses/Images/favicon.png">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <script type="text/javascript" src="JS/script.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <title>WereWolf - Match Configuration</title>
        
        <script type="text/javascript" src="JS/jquery.timepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="CSS/jquery.timepicker.css" />
    </head>
    <body>
    <div class="flex-container">
        <jsp:include page="../header.jsp"/>
        <article class="article">
            <h1>Match configuration</h1>
            <form method="POST" action="Controller_Match?action=add" onClick="return checkConfig" accept-charset="UTF-8">

                N° of players: 
                <p>Min
                    <select name="players_min">
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option> 
                    </select>

                    Max:
                    <select name="players_max">
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option> 
                    </select>
                </p>


                Day duration : <br>
                <p>From: 

                    <input id="day_from" name="day_from" type="text" class="time" style="width: 60px"/>
                    <script>
                         $(function() {
                             $('#day_from').timepicker();
                         });
                    </script>

                    <br>Duration: <select name="duration">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option> 
                    </select><br/>

                <p>
                    <br>

                    Powers probability (%): <select name="powers_probability">
                        <option value="0">0</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                        <option value="60">60</option>
                        <option value="70">70</option>
                        <option value="80">80</option>
                        <option value="90">90</option>
                        <option value="100">100</option> 
                    </select><br/>

                    Wolf initial proportion (%): <select name="werewolfsProp">
                        <option value="1">0</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                        <option value="60">60</option>
                        <option value="70">70</option>
                        <option value="80">80</option>
                        <option value="90">90</option>
                        <option value="100">100</option>
                    </select><br/>
                </p>
                <p>
                    Start hour : <input id="startHour" name="startHour" type="text" class="time" style="width: 60px"/>
                    <script>
                         $(function() {
                             $('#startHour').timepicker({ 'scrollDefault': '8:00am' });
                         });
                    </script>
                </p>

                <input type="submit" value="Create"/>
            </form>
        </article>
    </div>
        <jsp:include page="../footer.jsp"/>
    </body>
</html>