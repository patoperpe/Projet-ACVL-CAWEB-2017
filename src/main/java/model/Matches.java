/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utils.Dates;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Anna Paola
 */
public class Matches {
    private int iDurationDay;
    private Date oDate;
    private Date oHourStart;
    private int iNumPlayersMin;
    private int iNumPlayersMax;
    private float fPowerProbability;
    private float fWolfProportion;
    private int iID;
    private boolean started;
    private boolean finished;

    public Matches(int iDurationDay, Date oDate, Date oHourStart, int iNumPlayersMin, 
        int iNumPlayersMax, float fPowerProbability, float fWolfProportion, 
        int iID, boolean started, boolean finished) {
        this.iDurationDay = iDurationDay;
        this.oDate = oDate;
        this.oHourStart = oHourStart;
        this.iNumPlayersMin = iNumPlayersMin;
        this.iNumPlayersMax = iNumPlayersMax;
        this.fPowerProbability = fPowerProbability;
        this.fWolfProportion = fWolfProportion;
        this.iID = iID;
        this.started = started;
        this.finished = finished;
    }    

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public Matches(int iID) {
        this.iID = iID;
    }

    public Matches() {
        
    }

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }
    
    public void addPlayer(Players x){
        
    }

    public int getiDurationDay() {
        return iDurationDay;
    }

    public void setiDurationDay(int iDurationDay) {
        this.iDurationDay = iDurationDay;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }

    public Date getoHourStart() {
        return oHourStart;
    }

    public void setoHourStart(Date oHourStart) {
        this.oHourStart = oHourStart;
    }

    public int getiNumPlayersMin() {
        return iNumPlayersMin;
    }

    public void setiNumPlayersMin(int iNumPlayersMin) {
        this.iNumPlayersMin = iNumPlayersMin;
    }

    public int getiNumPlayersMax() {
        return iNumPlayersMax;
    }

    public void setiNumPlayersMax(int iNumPlayersMax) {
        this.iNumPlayersMax = iNumPlayersMax;
    }

    public float getfPowerProbability() {
        return fPowerProbability;
    }

    public void setfPowerProbability(float fPowerProbability) {
        this.fPowerProbability = fPowerProbability;
    }

    public float getfWolfProportion() {
        return fWolfProportion;
    }

    public void setfWolfProportion(float fWolfProportion) {
        this.fWolfProportion = fWolfProportion;
    }
    
    public Date getFinalDayHour(){
        return Dates.substractDate(oHourStart, iDurationDay, Calendar.HOUR_OF_DAY);
    }
}
