/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Anna Paola
 */
public class Designations {
    private Players oPlayerAcussed;
    private Players oPlayerAcusser;
    private Date oDate; 
    private Matches oMatch;
    private int iID;
    private boolean forWolf;
    private boolean completed;
    private boolean positive;
    private ArrayList<Votes> listVotes;
    
    public Designations(int iID) {
        this.iID = iID;
    }

    public Designations(Players oPlayerAcussed, Players oPlayerAcusser, Date oDate,
            Matches oMatch, int iID, boolean forWolf) {
        this.oPlayerAcussed = oPlayerAcussed;
        this.oPlayerAcusser = oPlayerAcusser;
        this.oDate = oDate;
        this.oMatch = oMatch;
        this.iID = iID;
        this.forWolf = forWolf;
    }

    public Designations() {
        
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isPositive() {
        return positive;
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    public boolean isForWolf() {
        return forWolf;
    }

    public void setForWolf(boolean forWolf) {
        this.forWolf = forWolf;
    }

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }

    public Matches getoMatch() {
        return oMatch;
    }

    public void setoMatch(Matches oMatch) {
        this.oMatch = oMatch;
    }
    
    public Players getoPlayerAcussed() {
        return oPlayerAcussed;
    }

    public void setoPlayerAcussed(Players oPlayerAcussed) {
        this.oPlayerAcussed = oPlayerAcussed;
    }

    public Players getoPlayerAcusser() {
        return oPlayerAcusser;
    }

    public void setoPlayerAcusser(Players oPlayerAcusser) {
        this.oPlayerAcusser = oPlayerAcusser;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }

    public ArrayList<Votes> getListVotes() {
        return listVotes;
    }

    public void setListVotes(ArrayList<Votes> listVotes) {
        this.listVotes = listVotes;
    }
    
    public int howManyPositivesVotes(){
        int iVotes = 0;
        if(listVotes != null && !listVotes.isEmpty()){
            for (Votes oVote : listVotes) {
                if(oVote.isIsPositive())
                    iVotes++;
            }
        }
        return iVotes;
    }
    
    public int howManyNegativesVotes(){
        int iVotes = 0;
        if(listVotes != null && !listVotes.isEmpty()){
            for (Votes oVote : listVotes) {
                if(!oVote.isIsPositive())
                    iVotes++;
            }
        }
        return iVotes;
    }
    
    public int howManyVotes(){
        if(listVotes != null && !listVotes.isEmpty())
            return listVotes.size();
        return 0;
    }
    
    public boolean hasVoted(Players oPlayer){
        if(listVotes == null || listVotes.isEmpty())
            return false;
        else{
            for (Votes oVote : listVotes) {
                if(oVote.getoPlayer().equals(oPlayer))
                    return true;
            }
            return false;
        }
    }
}

