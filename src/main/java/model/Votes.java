/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Anna Paola
 */
public class Votes {
    private Players oPlayer;
    private boolean isPositive;
    private Designations oDesignation;
    private int iID;

    public Votes(Players oPlayer, boolean isPositive, Designations oDesignation, int iID) {
        this.oPlayer = oPlayer;
        this.isPositive = isPositive;
        this.oDesignation = oDesignation;
        this.iID = iID;
    }

    public Votes() {
        
    }

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }
    public Players getoPlayer() {
        return oPlayer;
    }

    public void setoPlayer(Players oPlayer) {
        this.oPlayer = oPlayer;
    }

    public boolean isIsPositive() {
        return isPositive;
    }

    public void setIsPositive(boolean isPositive) {
        this.isPositive = isPositive;
    }

    public Designations getoDesignation() {
        return oDesignation;
    }

    public void setoDesignation(Designations oDesignation) {
        this.oDesignation = oDesignation;
    }
}
