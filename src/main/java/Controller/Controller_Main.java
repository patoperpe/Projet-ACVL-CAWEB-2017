package Controller;

import DAO.Clauses;
import DAO.ExceptionDAO;
import java.io.*;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;

/**
 * This is de main controller. It redirects request to the right sub-controller.
 * When no action is submitted, It redirects the request to the login view.
 */
@WebServlet(name = "Controller_Main", urlPatterns = {"/controller_Main"})
public class Controller_Main extends HttpServlet {

    @Resource(name = "jdbc/Projet-ACVL-CAWEB-2017")
    private DataSource ds;

    /* Error pages */
    private void invalidParameters(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ErrorControler.jsp").forward(request, response);        
    }

    private void erreurBD(HttpServletRequest request,
                HttpServletResponse response, ExceptionDAO e)
            throws ServletException, IOException {
        request.setAttribute("errorMessage", e.getMessage());
        request.getRequestDispatcher("/WEB-INF/ErrorDB.jsp").forward(request, response);
    }
  
    /**
     * Actions possibles en GET : afficher (correspond à l’absence du param), getOuvrage.
     * @param request
     * @param response
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    @Override
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        String sView = request.getParameter("view");
        if(sView == null){
            sView = "controller_Session";
        }
        response.sendRedirect(sView);
    }

}
