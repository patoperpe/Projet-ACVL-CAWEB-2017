/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utils.Dates;
import DAO.Clauses;
import DAO.DAOChats;
import DAO.DAOMatches;
import DAO.DAOPlayers;
import DAO.DAOUsers;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import model.Chats;
import model.Matches;
import model.Players;
import model.Users;

/**
 * This file controls all the aspects of create or join a Match. It redirects to
 * the main game view when all players are setted. It also make the raffle of
 * wolfs and citizens.
 */
@WebServlet(name = "Controller_Match", urlPatterns = {"/Controller_Match"})
public class Controller_Match extends HttpServlet {
    @Resource(name = "jdbc/Projet-ACVL-CAWEB-2017")
    private DataSource ds;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        DAOMatches oDAO;
        Clauses oClause;
        String sAction = request.getParameter("action");
        String sView = request.getParameter("view");
        if(sAction == null ){//If no action is requested
            if(sView == null)//If no view is requested.
            {
                HttpSession session = request.getSession(false);
                if(session == null || session.getAttribute("username") == null)//If user is not logged, it sends him to log in.
                    request.getRequestDispatcher("/Users/logIn.jsp").forward(request, response);
                else{//If it is logged.
                    oClause = new Clauses();
                    oClause.add("username", (String) session.getAttribute("username"));
                    DAOPlayers oDAOPlayers = new DAOPlayers(ds);
                    ArrayList<Players> lPlayeres = oDAOPlayers.getList(oClause);//Search all players of this user.
                    if(lPlayeres != null && !lPlayeres.isEmpty()){//If yes, Check if the match is started
                        oDAO = new DAOMatches(ds);
                        oClause = new Clauses();
                        for (Players oP : lPlayeres) {
                            oClause.addWithCondition("idMatches", oP.getoMatch().getiID()+"","OR");
                        }
                        oClause.addWithCondition("finished", "0","AND");
                        Matches oMatch = oDAO.get(oClause);//Get the current match started and not finished if exist.
                        if(oMatch == null)//If the user has not any player in a not finished match, show GameSelection
                            showGameSelection(request, response);
                        else{
                            if(oMatch.isStarted())//If the match has started
                                response.sendRedirect("Controller_Game?idMatches="+oMatch.getiID());
                            else{
                                oClause = new Clauses();
                                oClause.add("username", (String) session.getAttribute("username"));
                                oClause.add("idMatches", oMatch.getiID()+"");
                                Players oPlayer = oDAOPlayers.get(oClause);//Obtein the current player for the match obteined.
                                oClause = new Clauses();
                                oClause.add("idMatches", oPlayer.getoMatch().getiID()+"");
                                DAOPlayers oDAOP = new DAOPlayers(ds);
                                ArrayList<Players> listP = oDAOP.getList(oClause);//Get all players of the match.
                                int iMissingPlayers = oMatch.getiNumPlayersMin()- listP.size();//Find how many players are missing.
                                request.setAttribute("match", oMatch);
                                request.setAttribute("list", listP);
                                request.setAttribute("missingPlayers", ""+iMissingPlayers);
                                request.getRequestDispatcher("Game/waitingMatchStarts.jsp").forward(request, response);
                            }
                        }
                    }else{//If the user has not any match, show a list of available matches.
                        showGameSelection(request,response);
                    }
                }
            }
            else//Redirect to the requested view.
            {
                request.getRequestDispatcher("/Game/"+sView+".jsp").forward(request, response);
            }            
        }
        else{//If an action is requested.
            Matches oMatch = getMatchFromRequest(request);//Get the match from the request.
            if(oMatch == null)
                invalidParameters(request, response);
            DAOPlayers oDAOP = null;
            switch(sAction){//Sort which action was requested.
                case "add"://insert a new match into the game.
                        oDAO = new DAOMatches(ds);
                        oDAO.insert(oMatch);
                        Players oPlayer = new Players(new Users((String) request.getSession().getAttribute("username")));
                        oPlayer.setoMatch(oMatch);
                        if(oDAOP == null)
                            oDAOP = new DAOPlayers(ds);
                        oDAOP.insert(oPlayer);
                        oClause = new Clauses();
                        oClause.add("idMatches", oMatch.getiID()+"");
                        oDAO = new DAOMatches(ds);
                        oMatch = oDAO.get(oClause);
                        ArrayList<Players> listP = oDAOP.getList(oClause);
                        int iMissingPlayers = oMatch.getiNumPlayersMin()-listP.size();
                        request.setAttribute("missingPlayers", ""+iMissingPlayers);
                        request.setAttribute("list", listP);
                        request.setAttribute("match", oMatch);
                        request.getRequestDispatcher("Game/waitingMatchStarts.jsp").forward(request, response);
                    break;
                case "join":
                        HttpSession session = request.getSession(false);
                        if(session == null || session.getAttribute("username") == null)
                            request.getRequestDispatcher("/Users/logIn.jsp").forward(request, response);
                        else{
                            oMatch = getMatchFromRequest(request);
                            Players oPlayers = new Players(new Users((String) session.getAttribute("username")));
                            oPlayers.setoMatch(oMatch);
                            oDAOP = new DAOPlayers(ds);
                            oDAOP.insert(oPlayers);
                            oClause = new Clauses();
                            oClause.add("idMatches", oMatch.getiID()+"");
                            oDAO = new DAOMatches(ds);
                            oMatch = oDAO.get(oClause);
                            listP = oDAOP.getList(oClause);
                            iMissingPlayers = oMatch.getiNumPlayersMin()-listP.size();
                            if(iMissingPlayers > 0){//If more players are still missing.
                                request.setAttribute("missingPlayers", ""+iMissingPlayers);
                                request.setAttribute("list", listP);
                                request.setAttribute("match", oMatch);
                                request.getRequestDispatcher("Game/waitingMatchStarts.jsp").forward(request, response);
                            }else
                                setUpMatch(oMatch,listP,request,response);
                        }
                    break;
                case "leave":
                    oMatch = getMatchFromRequest(request);
                    oPlayer = new Players(new Users((String) request.getSession(false).getAttribute("username")));
                    oPlayer.setoMatch(oMatch);
                    oDAOP = new DAOPlayers(ds);
                    oDAOP.delete(oPlayer);
                    oDAO = new DAOMatches(ds);
                    ArrayList<Matches> list = oDAO.getList(null);//Collects all the matches.
                    request.setAttribute("list", list);//Put the list of matches into the request.
                    request.getRequestDispatcher("Game/gameSelection.jsp").forward(request, response);//send it.
                    break;
                default:
                    invalidParameters(request, response);
                    break;
            }
        }
    }

    private void invalidParameters(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ErrorControler.jsp").forward(request, response);        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Matches getMatchFromRequest(HttpServletRequest request) {
        String sID = request.getParameter("idMatches");
        String sProbability = request.getParameter("powers_probability");
        String sWolf = request.getParameter("werewolfsProp");
        String sDuration =request.getParameter("duration");
        String sPlayersMin = request.getParameter("players_min");
        String sPlayersMax = request.getParameter("players_max");
        String sHour = request.getParameter("day_from");
        Matches oMatch = new Matches();
        if(sID != null)
            oMatch.setiID(Integer.parseInt(sID));
        if(sProbability != null)
            oMatch.setfPowerProbability(Float.parseFloat(sProbability));
        if(sWolf != null)
            oMatch.setfWolfProportion(Float.parseFloat(sWolf));
        if(sDuration != null && !sDuration.equals("0")  && !sDuration.equals("Seleccionar"))
            oMatch.setiDurationDay(Integer.parseInt(sDuration));
        else
            oMatch.setiDurationDay(14);
        if(sPlayersMin != null)
            oMatch.setiNumPlayersMin(Integer.parseInt(sPlayersMin));
        if(sPlayersMax != null)
            oMatch.setiNumPlayersMax(Integer.parseInt(sPlayersMax));
        oMatch.setoDate(new Date());
        try {
            if(sHour != null && !sHour.isEmpty()){
                oMatch.setoHourStart(Dates.sdHoursAndMinutes.parse(sHour));
            }else{
                oMatch.setoHourStart(Dates.sdHoursAndMinutes.parse("8:00"));
            }
        } catch (ParseException ex) {
            Logger.getLogger(Controller_Match.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return oMatch;
    }

    private void setUpMatch(Matches oMatch, ArrayList<Players> listP, 
        HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOMatches oDAOMatches = new DAOMatches(ds);
        oMatch.setStarted(true);
        oDAOMatches.update(oMatch);//Put the match as started.
        DAOPlayers oDAOP = new DAOPlayers(ds);
        int iWolfs = (int) (oMatch.getfWolfProportion()*listP.size()/100);
        if(iWolfs == 0)//If proportion was 0, 1 player at least must be wolf.
            iWolfs = 1;
        for (Players oPlayer : listP) {//set if a player is wolf or citizen.
            if(iWolfs != 0){
                oPlayer.setIsWolf(true);
                iWolfs--;
            }
        }
        if(Math.random()<= oMatch.getfPowerProbability()){//Check if powers will be assigned.
            String[] sPowers = new String[]{"Spiritisme","Voyance","Contamination","Insomnie"};
            for (String sPower : sPowers) {//For each power.
                boolean loop = true;
                while(loop){//This loop is for repeat the process until the power is assigned.
                    int iPlayerNummber = (int) (Math.random()*(listP.size()-1));//Obtein the player number.
                    Players oPlayer = listP.get(iPlayerNummber);
                    if(oPlayer.getsPower() != null)//If player already has a power
                        continue;
                    boolean flag = true;
                    switch (sPower){
                        case "Contamination":
                            if(!oPlayer.isIsWolf())//Contamination is only for wolf, if the player is a citizen, do not assign it.
                                flag = false;
                            break;
                        case "Insomnie":
                            if(oPlayer.isIsWolf())//Insomnie is only for citizens, if the player is a wolf, do not assign it.
                                flag = false;
                            break;
                    }
                    if(flag){
                        oPlayer.setsPower(sPower);
                        loop = false;
                    }
                }
            }
        }
        for (Players oPlayer : listP) {//Update information of each player.
                oDAOP.update(oPlayer);
        }
        //Create the chat.
        Chats oChat = new Chats();
        oChat.setoMatch(oMatch);
        oChat.setoDate(new Date());
        oChat.setIsForWolf(false);
        DAOChats oDAOChats = new DAOChats(ds);
        oDAOChats.insert(oChat);
        oChat.setIsForWolf(true);
        oDAOChats.insert(oChat);
        response.sendRedirect("Controller_Game?idMatch="+ oMatch.getiID());
    }

    private void showGameSelection(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOMatches oDAO = new DAOMatches(ds);
        Clauses oClause = new Clauses();
        oClause.clear();
        oClause.add("finished", "0");
        ArrayList<Matches> listNotFinish = oDAO.getList(oClause);//Collects all the matches.
        request.setAttribute("listNotFinish", listNotFinish);//Put the list of matches into the request.
        oClause.clear();
        oClause.add("finished", "1");
        ArrayList<Matches> listFinish = oDAO.getList(oClause);//Collects all the matches.
        request.setAttribute("listFinish", listFinish);//Put the list of matches into the request.
        request.getRequestDispatcher("Game/gameSelection.jsp").forward(request, response);//send it.
    }

}
