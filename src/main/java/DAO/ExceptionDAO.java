
package DAO;

public class ExceptionDAO extends RuntimeException {

    public ExceptionDAO() {
    }

    public ExceptionDAO(String message) {
        super(message);
    }

    public ExceptionDAO(String message,Throwable cause) {
        super(message, cause);
    }

}
