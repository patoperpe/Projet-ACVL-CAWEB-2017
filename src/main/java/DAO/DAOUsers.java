/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Matches;
import model.Players;
import model.Users;

/**
 *
 * @author Anna Paola
 */
public class DAOUsers extends AbstractDataBaseDAO{
    
    public DAOUsers(DataSource ds) {
        super(ds);
    }
    public ArrayList<Users> getList(Clauses oClauses){
        ArrayList<Users> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM GAME_USERS");
            else
                rs = st.executeQuery("SELECT * FROM GAME_USERS " + oClauses.toString());
            while (rs.next()) {
                Users oUsers = new Users(rs.getString("username"),rs.getString("pass"));
                result.add(oUsers);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public boolean insert(Users oUser){
         try (
                Connection conn = getConn();
                PreparedStatement st = conn.prepareStatement("INSERT INTO GAME_USERS VALUES (?, ?)");) {
            st.setString(1, oUser.getsUsername());
            st.setString(2, oUser.getsPassword());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Users get(Clauses oClauses){
        Users result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM GAME_USERS");
            else
                rs = st.executeQuery("SELECT * FROM GAME_USERS " + oClauses.toString());
            while (rs.next()) {
                result = new Users(rs.getString("username"),rs.getString("pass"));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public void update(Users oUser){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE GAME_USERS SET \"password\" = ? WHERE \"username\" = ?");
	     ) {
            st.setString(1, oUser.getsPassword());
            st.setString(2, oUser.getsUsername());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Users oUser){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM GAME_USERS WHERE \"username\" = ? AND \"password\" = ?");
	     ) {
            st.setString(1, oUser.getsUsername());
            st.setString(2, oUser.getsPassword());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
}
