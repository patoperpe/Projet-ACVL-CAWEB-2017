/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.Dates;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Matches;
import model.Players;
import model.Users;

/**
 *
 * @author Anna Paola
 */
public class DAOPlayers extends AbstractDataBaseDAO{
    
    public DAOPlayers(DataSource ds) {
        super(ds);
    }
    public ArrayList<Players> getList(Clauses oClauses){
        ArrayList<Players> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Players");
            else
                rs = st.executeQuery("SELECT * FROM Players " + oClauses.toString());
            while (rs.next()) {
                Players oPlayer =
                    new Players(new Users(rs.getString("username")),
                        rs.getBoolean("isWolf"),rs.getString("power"),
                        rs.getBoolean("isAlive"),new Matches(rs.getInt("idMatches")));
                result.add(oPlayer);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public void insert(Players oPlayer){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO PLAYERS (\"idMatches\", \"isWolf\", \"isAlive\", \"power\", \"username\") "
                    + "VALUES (?, ?, ?, ?, ?)");
	     ) {
            st.setInt(1, oPlayer.getoMatch().getiID());
            st.setBoolean(2, oPlayer.isIsWolf());
            st.setBoolean(3, oPlayer.isIsAlived());
            if(oPlayer.getsPower() == null)
                st.setNull(4, 0);
            else
                st.setString(4, oPlayer.getsPower());
            st.setString(5, oPlayer.getoUser().getsUsername());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Players get(Clauses oClauses){
        Players result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM PLAYERS");
            else
                rs = st.executeQuery("SELECT * FROM PLAYERS " + oClauses.toString());
            while (rs.next()) {
                result =
                    new Players(new Users(rs.getString("username")),
                        rs.getBoolean("isWolf"),rs.getString("power"),rs.getBoolean("isAlive"),
                        new Matches(rs.getInt("idMatches")));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        return result;
    }
    
    public void update(Players oPlayer){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE PLAYERS SET \"idMatches\" = ?, \"isWolf\" = ?, \"isAlive\" = ?"
                    + ", \"power\" = ? WHERE \"username\" = ?");
	     ) {
            st.setInt(1, oPlayer.getoMatch().getiID());
            st.setBoolean(2, oPlayer.isIsWolf());
            st.setBoolean(3, oPlayer.isIsAlived());
            st.setString(4, oPlayer.getsPower());
            st.setString(5, oPlayer.getoUser().getsUsername());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Players oPlayer){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM PLAYERS WHERE \"username\" = ? AND \"idMatches\" = ? ");
	     ) {
            st.setString(1, oPlayer.getoUser().getsUsername());
            st.setInt(2, oPlayer.getoMatch().getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
}
