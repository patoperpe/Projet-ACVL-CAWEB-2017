/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.Dates;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import model.Chats;
import model.Matches;

/**
 *
 * @author Anna Paola
 */
public class DAOChats extends AbstractDataBaseDAO{
    
    public DAOChats(DataSource ds) {
        super(ds);
    }
    public ArrayList<Chats> getList(Clauses oClauses){
        ArrayList<Chats> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Chats");
            else
                rs = st.executeQuery("SELECT * FROM Chats " + oClauses.toString());
            while (rs.next()) {
                Chats oChat =
                    new Chats(rs.getDate("date")
                    , rs.getBoolean("isForWolf"), new Matches(rs.getInt("idMatches"))
                    ,rs.getInt("idChats"));
                result.add(oChat);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        if(result != null && !result.isEmpty()){
            DAOMessages oDAOMessages = new DAOMessages(dataSource);
            for (Chats oChat : result) {
                Clauses oClau = new Clauses();
                oClau.add("idChats", oChat.getiID()+"");
                oClau.setsOrder("date");
                oChat.setListMessages(oDAOMessages.getList(oClau));
            }
            
        }
	return result;
    }
    
    public void insert(Chats oChat){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO chats (\"idMatches\", \"isForWolf\","
                    + "\"date\",\"idChats\") VALUES (?, ?, ?, chat_sequence.nextval)");
	     ) {
            st.setInt(1, oChat.getoMatch().getiID());
            st.setBoolean(2, oChat.isIsForWolf());
            st.setDate(3,  new Date(oChat.getoDate().getTime()));
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Chats get(Clauses oClauses){
        Chats result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Chats");
            else
                rs = st.executeQuery("SELECT * FROM Chats " + oClauses.toString());
            while (rs.next()) {
                result =
                    new Chats(rs.getDate("date")
                    , rs.getBoolean("isForWolf"), new Matches(rs.getInt("idMatches"))
                    ,rs.getInt("idChats"));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        if(result != null){
            DAOMessages oDAOMessages = new DAOMessages(dataSource);
            oClauses = new Clauses();
            oClauses.add("idChats", result.getiID()+"");
            oClauses.setsOrder("date");
            result.setListMessages(oDAOMessages.getList(oClauses));
        }
	return result;
    }
    
    public void update(Chats oChat){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE TABLE chats  SET \"idMatches\" = ?, "
                    + "\"isForWolf\" = ?, date = ? WHERE \"idChats\" = ?");
	     ) {
            st.setInt(1, oChat.getoMatch().getiID());
            st.setBoolean(2, oChat.isIsForWolf());
            st.setDate(3,  new Date(oChat.getoDate().getTime()));
            st.setInt(4, oChat.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Chats oChat){
        try (
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM chats WHERE \"idChats\" = ?");
	     ) {
            st.setInt(1, oChat.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
    }
    
}
