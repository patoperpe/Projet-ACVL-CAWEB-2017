/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.Dates;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Designations;
import model.Matches;
import model.Players;
import model.Users;
/**
 *
 * @author Anna Paola
 */
public class DAODesignations extends AbstractDataBaseDAO{
    
    public DAODesignations(DataSource ds) {
        super(ds);
    }
    public ArrayList<Designations> getList(Clauses oClauses){
        ArrayList<Designations> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Designations");
            else
                rs = st.executeQuery("SELECT * FROM Designations " + oClauses.toString());
            while (rs.next()) {
                Designations oDesignation =
                    new Designations(new Players(new Users(rs.getString("playerAccused"))),
                        new Players(new Users(rs.getString("playerAccuser"))),
                        rs.getDate("date"), new Matches(rs.getInt("idMatches")),
                        rs.getInt("idDesignations"),rs.getBoolean("isForWolf"));
                result.add(oDesignation);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        if(!result.isEmpty()){
            DAOVotes oDAOVotes = new DAOVotes(dataSource);
            for (Designations oDesignation : result) {
                Clauses oClau = new Clauses();
                oClau.add("idDesignations",oDesignation.getiID()+"");
                oDesignation.setListVotes(oDAOVotes.getList(oClau));
            }
        }
	return result;
    }
    
    public void insert(Designations oDesignation){
            try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO designations (\"idMatches\", \"playerAccuser\", "
                    + "\"playerAccused\",\"date\", \"idDesignations\" " 
                    + ", \"isForWolf\")"
                    + "VALUES (?, ?, ?, ?, designation_sequence.nextval, ?)");
	     ) {
            st.setInt(1, oDesignation.getoMatch().getiID());
            st.setString(2, oDesignation.getoPlayerAcusser().getoUser().getsUsername());
            st.setString(3, oDesignation.getoPlayerAcussed().getoUser().getsUsername());
            st.setDate(4, new Date(oDesignation.getoDate().getTime()));
            st.setBoolean(5, oDesignation.isForWolf());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Designations get(Clauses oClauses){
        Designations result = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Designations");
            else
                rs = st.executeQuery("SELECT * FROM Designations " + oClauses.toString());
            while (rs.next()) {
                result =
                    new Designations(new Players(new Users(rs.getString("playerAccused"))),
                        new Players(new Users(rs.getString("playerAccuser"))),
                        rs.getDate("date"), new Matches(rs.getInt("idMatches")),
                        rs.getInt("idDesignations"),rs.getBoolean("isForWolf"));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
        if(result != null){
            DAOVotes oDAOVotes = new DAOVotes(dataSource);
            oClauses = new Clauses();
            oClauses.add("idDesignations", result.getiID()+"");
            result.setListVotes(oDAOVotes.getList(oClauses));
        }
	return result;
    }
    
    public void update(Designations oDesignation){
            try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE TABLE chats  SET \"idMatches\" = ?, \"playerAccuser\" = ?, "
                    + "\"playerAccused\" = ?, \"date\" = ?, \"isForWolf\" = ? "
                    + "WHERE \"idDesignations\" = ?");
	     ) {
            st.setInt(1, oDesignation.getoMatch().getiID());
            st.setString(2, oDesignation.getoPlayerAcusser().getoUser().getsUsername());
            st.setString(3, oDesignation.getoPlayerAcussed().getoUser().getsUsername());
            st.setDate(4, new Date(oDesignation.getoDate().getTime()));
            st.setBoolean(5, oDesignation.isForWolf());
            st.setInt(6, oDesignation.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Designations oDesignation){
        try (
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM designation WHERE \"idDesignations\" = ?");
	     ) {
            st.setInt(1, oDesignation.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
    }
    
}
