/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.Dates;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;
import model.Designations;
import model.Matches;
import model.Players;
import model.Users;

/**
 *
 * @author Anna Paola
 */
public class DAOMatches extends AbstractDataBaseDAO{
    
    public DAOMatches(DataSource ds) {
        super(ds);
    }
    public ArrayList<Matches> getList(Clauses oClauses){
        ArrayList<Matches> result = new ArrayList<>();
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Matches");
            else
                rs = st.executeQuery("SELECT * FROM Matches " + oClauses.toString());
            while (rs.next()) {
                Matches oMatch =
                    new Matches(rs.getInt("DurationDay"), rs.getDate("date"), 
                        rs.getDate("hourStart"), rs.getInt("cantPlayersMin"),
                        rs.getInt("cantPlayersMax"),rs.getFloat("powerProbability"), 
                        rs.getFloat("wolfProportion"), rs.getInt("idMatches")
                        ,rs.getBoolean("started"),rs.getBoolean("finished"));
                result.add(oMatch);
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
	return result;
    }
    
    public void insert(Matches oMatch){
         try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("INSERT INTO Matches (\"DurationDay\", \"date\", \"hourStart\", "
                    + "\"cantPlayersMin\",\"cantPlayersMax\",\"powerProbability\", "
                    + "\"wolfProportion\", \"idMatches\") VALUES (?, ?, ?, ?, ?, ?, ?, match_sequence.nextval)");
	     ) {
            st.setInt(1, oMatch.getiDurationDay());
            st.setDate(2, new Date( oMatch.getoDate().getTime()));
            st.setDate(3, new Date( oMatch.getoHourStart().getTime()));
            st.setInt(4, oMatch.getiNumPlayersMin());
            st.setInt(5, oMatch.getiNumPlayersMax());
            st.setFloat(6, oMatch.getfPowerProbability());
            st.setFloat(7, oMatch.getfWolfProportion());
            st.executeUpdate();
            ResultSet rs = st.executeQuery("select match_sequence.currval AS id from DUAL");
            while(rs.next())
                oMatch.setiID(rs.getInt("id"));
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public Matches get(Clauses oClauses){
        Matches oMatch = null;
        try (
	     Connection conn = getConn();
	     Statement st = conn.createStatement();
	     ) {
            ResultSet rs;
            if(oClauses == null)
                rs = st.executeQuery("SELECT * FROM Matches");
            else
                rs = st.executeQuery("SELECT * FROM Matches " + oClauses.toString());
            while (rs.next()) {
                oMatch =
                    new Matches(rs.getInt("DurationDay"), rs.getDate("date"), 
                        rs.getDate("hourStart"), rs.getInt("cantPlayersMin"),
                        rs.getInt("cantPlayersMax"),rs.getFloat("powerProbability"), 
                        rs.getFloat("wolfProportion"), rs.getInt("idMatches")
                        ,rs.getBoolean("started"),rs.getBoolean("finished"));
            }
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
	return oMatch;
    }
    
    public void update(Matches oMatch){
        try (
	     Connection conn = getConn();
	     PreparedStatement st = conn.prepareStatement
	       ("UPDATE Matches SET \"DurationDay\" = ?, \"date\" = ?, \"hourStart\" = ?,"
                    + " \"cantPlayersMin\" = ?, \"cantPlayersMax\" = ?,"
                    + " \"powerProbability\" = ?, \"wolfProportion\" = "
                    + "?, \"started\" = ?, \"finished\" = ? WHERE \"idMatches\" = ?");
	     ) {
            st.setInt(1, oMatch.getiDurationDay());
            st.setDate(2, new Date( oMatch.getoDate().getTime()));
            st.setDate(3, new Date( oMatch.getoHourStart().getTime()));
            st.setInt(4, oMatch.getiNumPlayersMin());
            st.setInt(5, oMatch.getiNumPlayersMax());
            st.setFloat(6, oMatch.getfPowerProbability());
            st.setFloat(7, oMatch.getfWolfProportion());
            st.setBoolean(8, oMatch.isStarted());
            st.setBoolean(9, oMatch.isFinished());
            st.setInt(10, oMatch.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
        }
    }
    
    public void delete(Matches oMatch){
        try (
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement
	       ("DELETE FROM Matches WHERE \"idMatches\" = ?");
	     ) {
            st.setInt(1, oMatch.getiID());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ExceptionDAO("Erreur BD " + e.getMessage(), e);
	}
    }
    
}
