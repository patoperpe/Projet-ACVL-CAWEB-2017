/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  patop
 * Created: 28/03/2017
 */

ALTER TABLE "CHATS" DROP CONSTRAINT "fk_Chats_idMatches";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_idMatches";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_playerAcusser";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_playerAcussed";

ALTER TABLE "PLAYERS" DROP CONSTRAINT "fk_Players_username";

ALTER TABLE "VOTES" DROP CONSTRAINT "fk_Votes_username";

ALTER TABLE "VOTES" DROP CONSTRAINT "fk_Votes_idDesignations";

ALTER TABLE "MESSAGES" DROP CONSTRAINT "fk_Messages_idChats";

ALTER TABLE "MESSAGES" DROP CONSTRAINT "fk_Messages_username";

ALTER TABLE "PLAYERS" DROP CONSTRAINT "fk_Players_idMatches";

DROP TABLE VOTES;

DROP TABLE DESIGNATIONS;

DROP TABLE MESSAGES;

DROP TABLE CHATS;

DROP TABLE MATCHES;

DROP TABLE PLAYERS;

DROP TABLE GAME_USERS;

DROP sequence match_sequence; 

DROP sequence vote_sequence; 

DROP sequence designation_sequence;

DROP sequence chat_sequence;  

DROP sequence message_sequence; 