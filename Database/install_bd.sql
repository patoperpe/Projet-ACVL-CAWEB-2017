ALTER TABLE "CHATS" DROP CONSTRAINT "fk_Chats_idMatches";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_idMatches";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_playerAcusser";

ALTER TABLE "DESIGNATIONS" DROP CONSTRAINT "fk_Designations_playerAcussed";

ALTER TABLE "PLAYERS" DROP CONSTRAINT "fk_Players_username";

ALTER TABLE "VOTES" DROP CONSTRAINT "fk_Votes_username";

ALTER TABLE "VOTES" DROP CONSTRAINT "fk_Votes_idDesignations";

ALTER TABLE "MESSAGES" DROP CONSTRAINT "fk_Messages_idChats";

ALTER TABLE "MESSAGES" DROP CONSTRAINT "fk_Messages_username";

ALTER TABLE "PLAYERS" DROP CONSTRAINT "fk_Players_idMatches";

DROP TABLE VOTES;

DROP TABLE DESIGNATIONS;

DROP TABLE MESSAGES;

DROP TABLE CHATS;

DROP TABLE MATCHES;

DROP TABLE PLAYERS;

DROP TABLE GAME_USERS;

DROP sequence match_sequence; 

DROP sequence vote_sequence; 

DROP sequence designation_sequence;

DROP sequence chat_sequence;  

DROP sequence message_sequence; 

CREATE TABLE "GAME_USERS" (

"username" VARCHAR(20) NOT NULL,

"pass" VARCHAR(20) NOT NULL,

PRIMARY KEY ("username") 

);



CREATE TABLE "MATCHES" (

"idMatches" INTEGER NOT NULL,

"DurationDay" INTEGER DEFAULT 14 NULL,

"hourStart" DATE DEFAULT TO_DATE('08:30:25', 'HH:MI:SS') NULL,

"wolfProportion" FLOAT DEFAULT 0.33 NULL,

"powerProbability" FLOAT DEFAULT 0 NULL,

"date" DATE DEFAULT SYSDATE NULL,

"cantPlayersMin" INTEGER NOT NULL,

"cantPlayersMax" INTEGER NOT NULL,

"started" INTEGER DEFAULT 0 NULL,

"finished" INTEGER DEFAULT 0 NULL,

PRIMARY KEY ("idMatches") 

);



CREATE TABLE "PLAYERS" (

"username" VARCHAR(20) NOT NULL,

"isWolf" INTEGER NOT NULL,

"power" VARCHAR(30) DEFAULT NULL NULL,

"isAlive" INTEGER DEFAULT 1 NULL,

"idMatches" INTEGER NOT NULL,

PRIMARY KEY ("username") 

);



CREATE TABLE "CHATS" (

"idChats" INTEGER NOT NULL,

"idMatches" INTEGER NOT NULL,

"isForWolf" INTEGER NOT NULL,

"date" DATE DEFAULT SYSDATE NOT NULL,

PRIMARY KEY ("idChats") 

);



CREATE TABLE "DESIGNATIONS" (

"idDesignations" INTEGER NOT NULL,

"idMatches" INTEGER NOT NULL,

"playerAccuser" VARCHAR(20) NOT NULL,

"playerAccused" VARCHAR(20) NOT NULL,

"date" DATE DEFAULT SYSDATE NULL,

"isForWolf" INTEGER DEFAULT 0 NOT NULL,

PRIMARY KEY ("idDesignations") 

);



CREATE TABLE "VOTES" (

"idVotes" INTEGER NOT NULL,

"username" VARCHAR(20) NOT NULL,

"idDesignations" INTEGER NOT NULL,

"isPositive" INTEGER NOT NULL,

PRIMARY KEY ("idVotes") 

);



CREATE TABLE "MESSAGES" (

"idMessages" INTEGER NOT NULL,

"idChats" INTEGER NOT NULL,

"username" VARCHAR(20) NOT NULL,

"date" DATE DEFAULT SYSDATE NULL,

"message" VARCHAR(40) NOT NULL,

PRIMARY KEY ("idMessages") 

);





ALTER TABLE "CHATS" ADD CONSTRAINT "fk_Chats_idMatches" FOREIGN KEY ("idMatches") REFERENCES "MATCHES" ("idMatches");

ALTER TABLE "DESIGNATIONS" ADD CONSTRAINT "fk_Designations_idMatches" FOREIGN KEY ("idMatches") REFERENCES "MATCHES" ("idMatches");

ALTER TABLE "DESIGNATIONS" ADD CONSTRAINT "fk_Designations_playerAccuser" FOREIGN KEY ("playerAccuser") REFERENCES "PLAYERS" ("username");

ALTER TABLE "DESIGNATIONS" ADD CONSTRAINT "fk_Designations_playerAccused" FOREIGN KEY ("playerAccused") REFERENCES "PLAYERS" ("username");

ALTER TABLE "PLAYERS" ADD CONSTRAINT "fk_Players_username" FOREIGN KEY ("username") REFERENCES "GAME_USERS" ("username");

ALTER TABLE "VOTES" ADD CONSTRAINT "fk_Votes_username" FOREIGN KEY ("username") REFERENCES "PLAYERS" ("username");

ALTER TABLE "VOTES" ADD CONSTRAINT "fk_Votes_idDesignations" FOREIGN KEY ("idDesignations") REFERENCES "DESIGNATIONS" ("idDesignations");

ALTER TABLE "MESSAGES" ADD CONSTRAINT "fk_Messages_idChats" FOREIGN KEY ("idChats") REFERENCES "CHATS" ("idChats");

ALTER TABLE "MESSAGES" ADD CONSTRAINT "fk_Messages_username" FOREIGN KEY ("username") REFERENCES "PLAYERS" ("username");

ALTER TABLE "PLAYERS" ADD CONSTRAINT "fk_Players_idMatches" FOREIGN KEY ("idMatches") REFERENCES "MATCHES" ("idMatches");



CREATE sequence match_sequence start with 1 increment by 1; 

CREATE sequence vote_sequence start with 1 increment by 1; 

CREATE sequence designation_sequence start with 1 increment by 1;

CREATE sequence chat_sequence start with 1 increment by 1;  

CREATE sequence message_sequence start with 1 increment by 1; 